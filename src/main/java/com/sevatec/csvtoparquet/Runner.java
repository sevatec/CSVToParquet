package com.sevatec.csvtoparquet;

import java.io.File;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			if(args.length != 2){
				System.out.println("CSV to Parquet requires an input file and output file to be specified.");
			}
			else{
				File csvFile = new File(args[0]);
				if(!(csvFile.exists() && !csvFile.isDirectory())){
					System.out.println("Specified CSV File does not exist.");
				}
				
				File outputParquetFile = new File(args[1]);
				
				ConvertUtils.convertCsvToParquet(csvFile, outputParquetFile);
			}
		}
		catch(Exception e){
			System.out.println("Exception caught:");
			System.out.println(e);
		}
	}
}
