/**
 * Copyright 2017 Sevatec, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.sevatec.csvtoparquet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.hadoop.fs.Path;

import parquet.Log;
import parquet.schema.MessageType;
import parquet.schema.MessageTypeParser;

public class ConvertUtils {

	private static final Log LOG = Log.getLog(ConvertUtils.class);

	public static final String CSV_DELIMITER= "|";

	private static String readFile(String path) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(path));
		StringBuilder stringBuilder = new StringBuilder();

		try {
			String line = null;
			//String ls = System.getProperty("line.separator");
			String ls = "\n";
			while ((line = reader.readLine()) != null ) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
		} finally {
			Utils.closeQuietly(reader);
		}

		return stringBuilder.toString();
	}

	public static String getSchema(File csvFile) throws IOException {
		String fileName = csvFile.getName().substring(
				0, csvFile.getName().length() - ".csv".length()) + ".schema";
		File schemaFile = new File(csvFile.getParentFile(), fileName);
		return readFile(schemaFile.getAbsolutePath());
	}

	public static void convertCsvToParquet(File csvFile, File outputParquetFile) throws IOException {
		convertCsvToParquet(csvFile, outputParquetFile, false);
	}

	public static void convertCsvToParquet(File csvFile, File outputParquetFile, boolean enableDictionary) throws IOException {
		LOG.info("Converting " + csvFile.getName() + " to " + outputParquetFile.getName());
		String rawSchema = getSchema(csvFile);
		if(outputParquetFile.exists()) {
			throw new IOException("Output file " + outputParquetFile.getAbsolutePath() + 
					" already exists");
		}

		Path path = new Path(outputParquetFile.toURI());

		MessageType schema = MessageTypeParser.parseMessageType(rawSchema);
		CsvParquetWriter writer = new CsvParquetWriter(path, schema, enableDictionary);

		BufferedReader br = new BufferedReader(new FileReader(csvFile));
		String line;
		int lineNumber = 0;
		try {
			while ((line = br.readLine()) != null) {
				String[] fields = line.split(Pattern.quote(CSV_DELIMITER));
				writer.write(Arrays.asList(fields));
				++lineNumber;
			}

			writer.close();
		} finally {
			LOG.info("Number of lines: " + lineNumber);
			Utils.closeQuietly(br);
		} 
	}
}
